// Replace these with your database connection details

// Replace this with the name of the table containing the location data
const tableName = 'cell_towers';

// Replace these with the column names for latitude and longitude
const latitudeColumn = 'latitude';
const longitudeColumn = 'longitude';
const signalStrengthColumn = 'signal_strength';

let map;
let markers = [];
let userMarkers = [];
let cellTowerInterval;
let userInterval;
let userMarkersVisible = true;

function toggleUserMarkers() {
  if (userMarkersVisible) {
    clearUserMarkers();
    clearInterval(userInterval);
    userMarkersVisible = false;
  } else {
    fetchAndDisplayUsers();
    setUserInterval(5000);
    userMarkersVisible = true;
  }
}

const toggleUsersBtn = document.querySelector('.toggle-users-btn');
toggleUsersBtn.addEventListener('click', toggleUserMarkers);

function initMap() {
  map = L.map('map', {
    fullscreenControl: true
  }).setView([40.7831, -73.9712], 14);
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

  fetchAndDisplayCellTowers();
  fetchAndDisplayUsers();
  setCellTowerInterval(5000);
  setUserInterval(5000);
}

function fetchAndDisplayCellTowers() {
    fetch(`/fetch-cell-towers?table=${tableName}&latitude=${latitudeColumn}&longitude=${longitudeColumn}&signal_strength=${signalStrengthColumn}`)
      .then(response => response.json())
      .then(data => {
        clearMarkers();
        data.forEach(item => {
          const signalStrength = item.signal_strength;
          const userCount = item.user_count;
          const ringColor = getColor(signalStrength);
          const radius = signalStrength * 1000;
          const radiusCircle = L.circle([item.latitude, item.longitude], {
            color: 'transparent',
            fillColor: ringColor,
            fillOpacity: 0.2,
            radius: radius,
            weight: 2,
            border: 0,
            zIndex: 100
          }).addTo(map);
          const marker = L.circleMarker([item.latitude, item.longitude], {
            fillColor: 'transparent',
            weight: 10,
            radius: 25,
            opacity: 0.8,
            color: ringColor,
            zIndex: 1000
          }).addTo(map);
          marker.bindTooltip(`Signal Strength: ${(signalStrength * 100).toFixed(2)}%\nUser Count: ${userCount}`, {
            permanent: false,
            direction: 'right',
            opacity: 0.8
          });
          markers.push(radiusCircle);
          markers.push(marker);
        });
      })
      .catch(error => console.error('Error fetching data:', error));
  }

function fetchAndDisplayUsers() {
    fetch(`/fetch-users?`)
      .then(response => response.json())
      .then(data => {
        clearUserMarkers();
        const userPositions = {};
        data.forEach(user => {
          const userId = user.user_id;
          const currentPosition = [user.latitude, user.longitude];
          const prevPosition = [user.prev_latitude, user.prev_longitude];
  
          if (!userPositions[userId]) {
            userPositions[userId] = [];
          }
  
          userPositions[userId].push(currentPosition);
  
          if (userPositions[userId].length > 1) {
            const prevPosition = userPositions[userId][userPositions[userId].length - 2];
            const angle = calculateAngle(prevPosition, currentPosition);
            const marker = L.marker(currentPosition, {
              icon: L.divIcon({
                className: 'arrow-icon',
                html: `<div style="transform: rotate(${angle}deg);">&#8594;</div>`,
                iconSize: [25, 25],
                iconAnchor: [10, 10],
                zIndex: 500
              })
            }).addTo(map);
            userMarkers.push(marker);
          }
        });
      })
      .catch(error => console.error('Error fetching users:', error));
  }
  
  function calculateAngle(prevPosition, currentPosition) {
    const [prevLat, prevLng] = prevPosition;
    const [currLat, currLng] = currentPosition;
  
    const dLng = currLng - prevLng;
    const dLat = currLat - prevLat;
  
    const angle = Math.atan2(dLng, dLat) * 180 / Math.PI;
  
    return angle;
  }

function getColor(signalStrength) {
  if (signalStrength <= 0.5) {
    return 'green';
  } else if (signalStrength > 0.5 && signalStrength < 0.85) {
    return 'orange';
  } else if (signalStrength >= 0.85) {
    return 'red';
  } else {
    return 'black';
  }
}

function clearMarkers() {
  markers.forEach(marker => marker.remove());
  markers = [];
}

function clearUserMarkers() {
  userMarkers.forEach(marker => marker.remove());
  userMarkers = [];
}

function setCellTowerInterval(interval) {
  clearInterval(cellTowerInterval);
  cellTowerInterval = setInterval(fetchAndDisplayCellTowers, interval);
}

function setUserInterval(interval) {
  clearInterval(userInterval);
  userInterval = setInterval(fetchAndDisplayUsers, interval);
}

const cellTowerIntervalInput = document.getElementById('cell-tower-interval');
const userIntervalInput = document.getElementById('user-interval');

cellTowerIntervalInput.addEventListener('input', () => {
  const interval = parseInt(cellTowerIntervalInput.value, 10);
  if (!isNaN(interval) && interval >= 1000) {
    setCellTowerInterval(interval);
  }
});

userIntervalInput.addEventListener('input', () => {
  const interval = parseInt(userIntervalInput.value, 10);
  if (!isNaN(interval) && interval >= 1000) {
    setUserInterval(interval);
  }
});

window.initMap();