# SingleStore Telco Demo
## Build
* Clone this repository
* Build Docker Container `docker buildx build --no-cache -f ./Dockerfile -t singlestore-demo/telco .`

## Run
* Run the Container `docker run -p 3000:3000 --env-file .env -d --name telco singlestore-demo/telco`
* Open your browser and point it to [localhost:3000]

 [localhost:3000]: http://localhost:3000