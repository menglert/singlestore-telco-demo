const express = require('express');
const mysql = require('mysql');
const cors = require('cors');
const app = express();

const dbHost = process.env.DB_HOST;
const dbUser = process.env.DB_USER;
const dbPassword = process.env.DB_PASSWORD;
const dbDatabase = process.env.DB_DATABASE;

app.use(cors());
app.use(cors({
    origin: 'http://127.0.0.1'
}));

// Function to update cell towers
function updateCellTowers() {
    const connection = mysql.createConnection({
        host: dbHost,
        user: dbUser,
        password: dbPassword,
        database: dbDatabase
    });

    connection.query(`CALL update_signal_strength();`, (error) => {
        if (error) {
            console.error('Error updating cell towers:', error);
        } else {
            console.log('Cell towers updated successfully');
        }
    });

    connection.end();
}

// Function to update users
function updateUsers() {
    const connection = mysql.createConnection({
        host: dbHost,
        user: dbUser,
        password: dbPassword,
        database: dbDatabase
    });

    connection.query(`CALL add_users();`, (error) => {
        if (error) {
            console.error('Error updating users:', error);
        } else {
            console.log('Users updated successfully');
        }
    });

    connection.end();
}

function deleteOldUsers() {
    const connection = mysql.createConnection({
        host: dbHost,
        user: dbUser,
        password: dbPassword,
        database: dbDatabase
    });

    connection.query(`CALL delete_old_entries();`, (error) => {
        if (error) {
            console.error('Error updating users:', error);
        } else {
            console.log('Old Users removed successfully');
        }
    });

    connection.end();
}

// Run the update functions every 2 seconds
setInterval(updateCellTowers, 2000);
setInterval(updateUsers, 2000);

setInterval(deleteOldUsers, 60000);

app.get('/fetch-cell-towers', (req, res) => {
    const { table, latitude, longitude, signal_strength } = req.query;
  
    var connection = mysql.createConnection({
      host: dbHost,
      user: dbUser,
      password: dbPassword,
      database: dbDatabase
    });
  
    connection.query(`SELECT ${latitude}, ${longitude}, ${signal_strength} FROM ${table} ORDER BY id`, (error, results, fields) => {
      if (error) {
        console.error('Error fetching cell tower data:', error);
        res.status(500).send('Error fetching cell tower data');
        return;
      }
  
      const cellTowerData = results;
  
      // Fetch the user count for each cell tower
      const promises = cellTowerData.map(tower => {
        return new Promise((resolve, reject) => {
          connection.query(`
            SELECT COUNT(*) AS user_count
            FROM users
            WHERE
              SQRT(POWER(${latitude} - latitude, 2) + POWER(${longitude} - longitude, 2)) <= 500
          `, [tower[latitude], tower[longitude]], (error, userCountResult, fields) => {
            if (error) {
              reject(error);
            } else {
              resolve({
                ...tower,
                user_count: userCountResult[0].user_count
              });
            }
          });
        });
      });
  
      Promise.all(promises)
        .then(data => {
          res.json(data);
        })
        .catch(error => {
          console.error('Error fetching user count:', error);
          res.status(500).send('Error fetching user count');
        });
  
      connection.end();
    });
  });

app.get('/fetch-users', (req, res) => {

    var connection = mysql.createConnection({
        host: dbHost,
        user: dbUser,
        password: dbPassword,
        database: dbDatabase
    });

    connection.query(`
    WITH RankedData AS (
        SELECT                user_id,
            latitude,
            longitude,
            timestamp,
            ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY timestamp DESC) AS row_num
        FROM users
    )
    
    SELECT 
        user_id,
        latitude,
        longitude,
        timestamp,
        LAG(latitude, 1) OVER (PARTITION BY user_id ORDER BY timestamp DESC) AS prev_latitude,
        LAG(longitude, 1) OVER (PARTITION BY user_id ORDER BY timestamp DESC) AS prev_longitude
    FROM RankedData
    WHERE row_num <= 2
    ORDER BY user_id, timestamp DESC;
    `, (error, results, fields) => {
        if (error) {
            console.error('Error fetching users:', error);
            res.status(500).send('Error fetching users');
            return;
        }

        res.json(results);
    });

    connection.end();
});

app.use(express.static('.'));

app.listen(3000, () => {
  console.log('Server listening on port 3000');
});