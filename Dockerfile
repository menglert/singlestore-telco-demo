FROM node:14

# Set the working directory
WORKDIR /app

# Copy the package.json and package-lock.json files
COPY package*.json ./

# Install the dependencies
RUN npm install

# Copy the server.js file
COPY server.js .

# Copy the frontend files
COPY index.html .
COPY script.js .

# Set the environment variables for the database connection
ENV DB_HOST=$dbHost
ENV DB_USER=$dbUser
ENV DB_PASSWORD=$dbPassword
ENV DB_DATABASE=$dbDatabase

# Expose the port for the server
EXPOSE 3000

# Start the server
CMD ["node", "server.js"]